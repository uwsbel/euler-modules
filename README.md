# Euler Environment Modules and Documentation

This repository contains version controlled copies of the Lua source code for most[^1] of Euler's Environment Modules.

It is also home to the in-tree documentation for selected modules. The documentation is configured in a wiki-like format for rendering in the VCS web interface, and will conveniently _not_ interfere with `Lmod` when cloned to Euler.

## Verification

All commits to this repository must be GPG signed and come from verified Euler maintainers. Non-compliant commits will be discarded.

---

[^1]: Modules provided by third party software will not be published here for licensing reasons. Documentation may still be made available in the tree to reflect the module's location on Euler.