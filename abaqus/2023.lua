
help([[
For module documentation, please visit https://gitlab.com/uwsbel/euler-modules
]])

local name = "abaqus"
local vers = "2023"
-- local arch = string.gsub(subprocess("uname -m"), "%s+", "")
local root = "/opt/apps/" .. name .. "/commands/" .. vers

whatis("SIMULIA Abaqus")
whatis("2023")


prepend_path("PATH", root)

