help([[
\tAnaconda3 module for use with 'Lmod' package:
]])

local           vers = "23.3.1"
local sysarch = subprocess('uname -m'):gsub('%s+', '')
local           root = "/opt/apps/anaconda/mini/" .. sysarch .. "/" .. vers

if not userInGroups("euler-courses") then
	LmodWarning("This module will be available for coursework only beginning on June 3rd, 2024!\nSee https://kb.wisc.edu/cae/137177#supported-tools for more information.")
end

local           active_shell = myShellName()

if active_shell == "bash" then
        set_shell_function("bootstrap_conda", "eval \"$(command conda 'shell.bash' 'hook' 2> /dev/null)\"", "")
        set_shell_function("bootstrap-conda", "eval \"$(command conda 'shell.bash' 'hook' 2> /dev/null)\"", "")
elseif  active_shell == "fish" then
        set_shell_function("bootstrap_conda", "eval conda \"shell.fish\" \"hook\" $argv | source", "")
        set_shell_function("bootstrap-conda", "eval conda \"shell.fish\" \"hook\" $argv | source", "")
elseif  active_shell == "zsh" then
        set_shell_function("bootstrap_conda", "eval \"$('conda' 'shell.zsh' 'hook' 2> /dev/null)\"", "")
        set_shell_function("bootstrap-conda", "eval \"$('conda' 'shell.zsh' 'hook' 2> /dev/null)\"", "")
end

prepend_path(           "PATH", root .. "/bin")
prepend_path(           "LD_LIBRARY_PATH", root .. "/lib")
