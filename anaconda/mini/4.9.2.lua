help([[
\tAnaconda3 module for use with 'Lmod' package:
]])

local           vers = "4.9.2"
local sysarch = subprocess('uname -m'):gsub('%s+', '')
local           root = "/opt/apps/anaconda/mini/" .. sysarch .. "/" .. vers

if not userInGroups("euler-courses") then
	LmodError("This module is available for coursework only!")
end

local           active_shell = myShellName()

if active_shell == "bash" then
        set_shell_function("bootstrap_conda", "eval \"$(command conda 'shell.bash' 'hook' 2> /dev/null)\"", "")
elseif  active_shell == "fish" then
        set_shell_function("bootstrap_conda", "eval conda \"shell.fish\" \"hook\" $argv | source", "")
elseif  active_shell == "zsh" then
        set_shell_function("bootstrap_conda", "eval \"$('conda' 'shell.zsh' 'hook' 2> /dev/null)\"", "")
end

prepend_path(           "PATH", root .. "/bin")
prepend_path(           "LD_LIBRARY_PATH", root .. "/lib64")
