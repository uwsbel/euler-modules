help([[
For module documentation, please visit https://gitlab.com/uwsbel/euler-modules
]])

local name = "openblas"
local arch = string.gsub(subprocess("uname -m"), "%s+", "")
local vers = "0.3.19"
local root = "/opt/apps/" .. name .. "/" .. arch .. "/" .. vers


-------------------------
-- Include Directories --
-------------------------

prepend_path("CPATH",			root .. "/include")

------------------------------------
-- Build-Time Library Directories --
------------------------------------

prepend_path("LIBRARY_PATH",	root .. "/lib64")

----------------------------------
-- Run-Time Library Directories --
----------------------------------

prepend_path("LD_LIBRARY_PATH",	root .. "/lib64")

------------------
-- CMake Config -- 
------------------

prepend_path("CMAKE_PREFIX_PATH", root .. "/share/cmake")


