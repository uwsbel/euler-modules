
help([[
For module documentation, please visit https://gitlab.com/uwsbel/euler-modules
]])

local name = "blaze"
local vers = "3.8"
-- local arch = string.gsub(subprocess("uname -m"), "%s+", "")
-- local root = "/opt/apps/" .. name .. "/" .. arch .. "/" .. vers
local root = "/opt/apps/" .. name .. "/" .. vers

whatis("blaze")
whatis("Version 3.8")

prepend_path("INCLUDE_PATH", root .. "/include")
prepend_path("CPATH", root .. "/include")

prepend_path("CMAKE_PREFIX_PATH", root .. "/share/blaze/cmake")