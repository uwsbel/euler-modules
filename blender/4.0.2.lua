help([[
For module documentation, please visit https://git.doit.wisc.edu/engr/me/wacc/euler-modules
]])

local name = "blender"
local arch = string.gsub(subprocess("uname -m"), "%s+", "")
local vers = "4.0.2"
local root = pathJoin("/opt/apps/", name, arch, vers)

------------------------
-- Binary Directories --
------------------------

prepend_path("PATH",			root)

