
help([[
For module documentation, please visit [TBA]
]])

local name = "boost"
local vers = "1.85.0"
local arch = string.gsub(subprocess("uname -m"), "%s+", "")
local root = "/opt/apps/" .. name .. "/" .. arch .. "/" .. vers

whatis("Boost C++ Libraries")
whatis("Version 1.85.0")


prepend_path("LD_LIBRARY_PATH",    root .. "/lib")
prepend_path("LIBRARY_PATH",    root .. "/lib")
prepend_path("CPATH", root .. "/include")

-- This probably doesn't do anything from the environment, but someone might want it
prepend_path("CMAKE_MODULE_PATH", root .. "/lib")

