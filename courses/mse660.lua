
help([[
For module documentation, please visit https://gitlab.com/uwsbel/euler-modules
]])

local group = "courses/mse660"
local name = "MUPRO"
local root = "/opt/apps/" .. group .. "/" .. name

if not userInGroups("euler-courses", "euler-hulab") then
	LmodError("This module is restricted to users enrolled MSE660 only!")
end

whatis("MSE660 MUPRO Software")
whatis("2022-01")

depends_on("intel/compiler")
depends_on("intel/mkl")
depends_on("fftw/impi/3.3.10")
depends_on("gcc/9.4.0")

prepend_path("PATH", root .. "/bin")


