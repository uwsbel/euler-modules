
help([[
For module documentation, please visit https://gitlab.com/uwsbel/euler-modules
]])

local name = "eigen"
local vers = "3.3.9"
-- local arch = string.gsub(subprocess("uname -m"), "%s+", "")
-- local root = "/opt/apps/" .. name .. "/" .. arch .. "/" .. vers
local root = "/opt/apps/" .. name .. "/" .. vers

whatis("Eigen")
whatis("Version 3.3.9")

prepend_path("INCLUDE_PATH", root .. "/include")
prepend_path("CPATH", root .. "/include")

prepend_path("PKG_CONFIG_PATH", root .. "/share/pkgconfig")
prepend_path("CMAKE_PREFIX_PATH", root .. "/share/pkgconfig")
prepend_path("CMAKE_PREFIX_PATH", root .. "/share/eigen3/cmake")
