help([[
\tEmscripten module for use with 'Lmod' package:
]])

local           vers = "2.0.30"
local sysarch = subprocess('uname -m'):gsub('%s+', '')
local           root = "/opt/apps/emscripten/" .. sysarch .. "/" .. vers


prepend_path("PATH", root)
prepend_path("PATH", root .. "/upstream/emscripten")
prepend_path("PATH", root .. "/node/14.15.5_64bit/bin")

setenv("EMSDK", root)
setenv("EM_CONFIG", root .. "/.emscripten")
setenv("EMSDK_NODE", root .. "/node/14.15.5_64bit/bin/node")

