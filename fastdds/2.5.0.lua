
help([[
For module documentation, please visit https://gitlab.com/uwsbel/euler-modules
]])

local name = "fastdds"
local vers = "2.5.0"
local arch = string.gsub(subprocess("uname -m"), "%s+", "")
local root = "/opt/apps/" .. name .. "/" .. arch .. "/" .. vers
local depends = root .. "/depends/foonathan_memory/0.7"

whatis("Fast-DDS")
whatis("Version 2.5.0")

prepend_path("PATH", root .. "/bin")
prepend_path("LIBRARY_PATH", root .. "/lib")
prepend_path("LD_LIBRARY_PATH", root .. "/lib")
prepend_path("INCLUDE_PATH", root .. "/include")
prepend_path("CPATH", root .. "/include")

prepend_path("PKG_CONFIG_PATH", root .. "/lib/cmake/fastcdr")
prepend_path("PKG_CONFIG_PATH", root .. "/share/fastrtps/cmake")
prepend_path("PKG_CONFIG_PATH", depends .. "/lib64/foonathan_memory/cmake")
prepend_path("CMAKE_PREFIX_PATH", root .. "/lib/cmake/fastcdr")
prepend_path("CMAKE_PREFIX_PATH", root .. "/share/fastrtps/cmake")
prepend_path("CMAKE_PREFIX_PATH", depends .. "/lib64/foonathan_memory/cmake")

