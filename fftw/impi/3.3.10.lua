
help([[
For module documentation, please visit https://gitlab.com/uwsbel/euler-modules
]])

local name = "fftw"
local vers = "3.3.10"
local arch = string.gsub(subprocess("uname -m"), "%s+", "")
local variant = "impi"
local root = "/opt/apps/" .. name .. "/" .. arch .. "/" .. variant .. "/" .. vers

whatis("FFTW for Intel MPI")
whatis("Version 3.3.10")

setenv("FFTW_ROOT", root)

prepend_path("PATH", root .. "/bin")
prepend_path("LIBRARY_PATH", root .. "/lib")
prepend_path("LD_LIBRARY_PATH", root .. "/lib")
prepend_path("MANPATH", root .. "/share/man")
prepend_path("INCLUDE_PATH", root .. "/include")
prepend_path("CPATH", root .. "/include")

prepend_path("PKG_CONFIG_PATH", root .. "/lib/pkgconfig")
prepend_path("CMAKE_PREFIX_PATH", root .. "/lib/cmake")

family("fftw")

depends_on("intel/mpi")

