
help([[
For module documentation, please visit https://gitlab.com/uwsbel/euler-modules
]])

local name = "fftw"
local vers = "3.3.10f-2024.0"
local arch = string.gsub(subprocess("uname -m"), "%s+", "")
local variant = "impi"
local root = "/opt/apps/" .. name .. "/" .. arch .. "/" .. variant .. "/" .. vers

whatis("FFTW for Intel MPI")
whatis("Version 3.3.10 Single-Precision")

setenv("FFTW_ROOT", root)

prepend_path("PATH", root .. "/bin")
prepend_path("LIBRARY_PATH", root .. "/lib")
prepend_path("LD_LIBRARY_PATH", root .. "/lib")
prepend_path("MANPATH", root .. "/share/man")
prepend_path("INCLUDE_PATH", root .. "/include")
prepend_path("CPATH", root .. "/include")

prepend_path("PKG_CONFIG_PATH", root .. "/lib/pkgconfig")
prepend_path("CMAKE_PREFIX_PATH", root .. "/lib/cmake")

family("fftw")

depends_on("mpi/impi/2021.11")
depends_on("intel/compiler-rt/2024.0.0")
depends_on("intel/ifort/2024.0.0")
depends_on("intel/tbb/2021.11")
depends_on("intel/oclfpga/2024.0.0")
depends_on("intel/compiler/2024.0.0")

