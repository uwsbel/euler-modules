
help([[
For module documentation, please visit https://gitlab.com/uwsbel/euler-modules
]])

local name = "gcc"
local vers = "8.5.0"
local arch = string.gsub(subprocess("uname -m"), "%s+", "")
local root = "/opt/apps/" .. name .. "/" .. arch .. "/" .. vers

whatis("GNU Compiler Collection")
whatis("Version " .. vers)


prepend_path("PATH",    root .. "/bin")

local cu_ = ""
if string.find(myModuleVersion(), "cuda") then
	cu_ = "CU_"
end

pushenv(cu_ .. "CC",   root .. "/bin/gcc")
pushenv(cu_ .. "CXX",  root .. "/bin/g++")
pushenv(cu_ .. "FC",   root .. "/bin/gfortran")
