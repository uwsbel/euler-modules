help([[
For module documentation, please visit https://git.doit.wisc.edu/engr/me/wacc/euler-modules
]])

local name = "gromacs"
local arch = string.gsub(subprocess("uname -m"), "%s+", "")
local vers = "2023.3"
local variant = "cuda-12.2.0-mpich"
local root = pathJoin("/opt/apps", name, arch, variant, vers)


depends_on("nvidia/cuda/12.2.0")
depends_on("mpi/mpich/4.0.2")
depends_on("gcc/12.2.0")

------------------------
-- Binary Directories --
------------------------

prepend_path("PATH",			root .. "/bin")

-------------------------
-- Include Directories --
-------------------------

prepend_path("CPATH",			root .. "/include")

------------------------------------
-- Build-Time Library Directories --
------------------------------------

prepend_path("LIBRARY_PATH",	root .. "/lib64")

----------------------------------
-- Run-Time Library Directories --
----------------------------------

prepend_path("LD_LIBRARY_PATH",	root .. "/lib64")

----------------------------------
-- Additional Environment Setup --
----------------------------------

setenv("GROMACS_ENV_SCRIPTS", root .. "/bin")


local active_shell = myShellName()

LmodMessage("Loading GMXRC...")
if active_shell == "bash" or active_shell == "zsh" or active_shell == "csh" then
	source_sh(active_shell, root .. "/bin/GMXRC." .. active_shell)
elseif active_shell == "tcsh" then
	source_sh(active_shell, root .. "/bin/GMXRC." .. "csh")
elseif active_shell == "fish" then
	LmodError("ERROR: GROMACS does not support fish!")
else
	LmodError("ERROR: Your shell is not supported on Euler!")
end
LmodMessage("Done!")


