
help([[
For help with modules, please contact euler-support@engr.wisc.edu
]])

local name = "hdf5"
local vers = "1.14.4-2"
local variant = "mpich"
local arch = string.gsub(subprocess("uname -m"), "%s+", "")
local root = "/opt/apps/" .. name .. "/" .. arch .. "/" .. variant .. "/" .. vers


depends_on("szip/2.1.1")
depends_on("mpi/mpich/4.0.2")

whatis("HDF5")
whatis("Version 1.14.4-2")


prepend_path("PATH",            root .. "/bin")
prepend_path("CPATH",           root .. "/include")
prepend_path("LD_LIBRARY_PATH", root .. "/lib")
prepend_path("LIBRARY_PATH",    root .. "/lib")



