
help([[
For module documentation, please visit https://gitlab.com/uwsbel/euler-modules
]])

local name = "julia"
local vers = "1.6.1"
local arch = string.gsub(subprocess("uname -m"), "%s+", "")
local root = "/opt/apps/" .. name .. "/" .. arch .. "/" .. vers

whatis("Julia")
whatis("Version 1.6.1")

prepend_path("PATH", root .. "/bin")
prepend_path("LIBRARY_PATH", root .. "/lib")
prepend_path("LD_LIBRARY_PATH", root .. "/lib")
prepend_path("MANPATH", root .. "/share/man")
prepend_path("INCLUDE_PATH", root .. "/include")
prepend_path("CPATH", root .. "/include")


pushenv("JULIA_BINDIR", root .. "/bin")

-- JULIA_EXCLUSIVE=0 might be needed to ensure cooperation with Slurm
-- https://docs.julialang.org/en/v1/manual/environment-variables/#JULIA_EXCLUSIVE
-- pushenv("JULIA_EXCLUSIVE", 0)

