help([[
For module documentation, please visit https://git.doit.wisc.edu/engr/me/wacc/euler-modules
]])

local name = "lammps"
local arch = string.gsub(subprocess("uname -m"), "%s+", "")
local vers = "2023.08.02u1"
local variant = "generic"
local root = pathJoin("/opt/apps/", name, arch, variant, vers)

------------------------
-- Binary Directories --
------------------------

prepend_path("PATH",			root .. "/bin")

-------------------------
-- Include Directories --
-------------------------

prepend_path("CPATH",			root .. "/include")

------------------------------------
-- Build-Time Library Directories --
------------------------------------

prepend_path("LIBRARY_PATH",	root .. "/lib64")

----------------------------------
-- Run-Time Library Directories --
----------------------------------

prepend_path("LD_LIBRARY_PATH",	root .. "/lib64")

----------------------------------
-- Additional Environment Setup --
----------------------------------

local potentials = os.getenv("LAMMPS_POTENTIALS")

if potentials == nil then
	setenv("LAMMPS_POTENTIALS", root .. "/share/lammps/potentials")
end


local msi2lmp = os.getenv("MSI2LMP_LIBRARY")

if msi2lmp == nil then
	setenv("MSI2LMP_LIBRARY", root .. "/share/lammps/frc_files")
end


