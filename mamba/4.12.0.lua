help([[
\tMiniForge with mamba module for use with 'Lmod' package:
]])

local           vers = "4.12.0"
local sysarch = subprocess('uname -m'):gsub('%s+', '')
local           root = "/opt/apps/mamba/" .. sysarch .. "/" .. vers

local           active_shell = myShellName()

if active_shell == "bash" then
        set_shell_function("bootstrap_conda", "eval \"$("..root.."/bin/conda 'shell.bash' 'hook' 2> /dev/null)\"", "")
elseif  active_shell == "fish" then
        set_shell_function("bootstrap_conda", "eval "..root.."/bin/conda \"shell.fish\" \"hook\" $argv | source", "")
elseif  active_shell == "zsh" then
        set_shell_function("bootstrap_conda", "eval \"$('"..root.."/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)\"", "")
end

prepend_path(           "PATH", root .. "/bin")
prepend_path(           "LD_LIBRARY_PATH", root .. "/lib")
