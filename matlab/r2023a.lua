
help([[
For module documentation, please visit https://gitlab.com/uwsbel/euler-modules
]])

local name = "matlab"
local vers = "r2023a"
local root = "/opt/apps/" .. name .. "/" .. vers

whatis("MATLAB")
whatis("R2023a")

setenv("MLM_LICENSE_FILE", "1705@license-1.cae.wisc.edu")
prepend_path("PATH",    root .. "/bin")

