
help([[
For module documentation, please visit https://gitlab.com/uwsbel/euler-modules
]])

local name = "mpich"
local vers = "3.4.3"
local arch = string.gsub(subprocess("uname -m"), "%s+", "")
local root = "/opt/apps/" .. name .. "/" .. arch .. "/" .. vers

whatis("MPICH")
whatis("Version 3.4.3")

prepend_path("PATH", root .. "/bin")
prepend_path("LIBRARY_PATH", root .. "/lib")
prepend_path("LD_LIBRARY_PATH", root .. "/lib")
prepend_path("MANPATH", root .. "/share/man")
prepend_path("INCLUDE_PATH", root .. "/include")
prepend_path("CPATH", root .. "/include")

pushenv("MPI_HOME", root)
pushenv("MPI_BIN", root .. "/bin")
pushenv("MPI_SYSCONFIG", root .. "/etc")
pushenv("MPI_INCLUDE", root .. "/include")
pushenv("MPI_LIB", root .. "/lib")
pushenv("MPI_MAN", root .. "/share/man")
pushenv("MPI_COMPILER", "mpich-" .. arch)
pushenv("MPI_SUFFIX", "_mpich")

prepend_path("PKG_CONFIG_PATH", root .. "/lib/pkgconfig")
prepend_path("CMAKE_PREFIX_PATH", root .. "/lib/pkgconfig")

family("mpi")
