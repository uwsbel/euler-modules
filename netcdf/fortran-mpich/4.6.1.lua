
help([[
For help with modules, please contact euler-support@engr.wisc.edu
]])

local name = "netcdf"
local vers = "4.6.1"
local variant = "fortran-mpich"
local arch = string.gsub(subprocess("uname -m"), "%s+", "")
local root = "/opt/apps/" .. name .. "/" .. arch .. "/" .. variant .. "/" .. vers

whatis("netCDF-fortran")
whatis("Version 4.6.1")

depends_on("mpi/mpich/4.0.2")
depends_on("hdf5/mpich/1.14.4")
depends_on("netcdf/c-mpich/4.9.2")

prepend_path("PATH",            root .. "/bin")
prepend_path("CPATH",           root .. "/include")
prepend_path("LD_LIBRARY_PATH", root .. "/lib64")
prepend_path("LIBRARY_PATH",    root .. "/lib64")



