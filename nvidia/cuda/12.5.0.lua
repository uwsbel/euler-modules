help([[
For help with modules, please contact euler-support@engr.wisc.edu
]])

local name = "cuda"
local arch = string.gsub(subprocess("uname -m"), "%s+", "")
local vers = "12.5.0"
local variant = "default"
local root = "/opt/apps/" .. name .. "/" .. arch .. "/" .. vers .. "/" .. variant


------------------------
-- Binary Directories --
------------------------

prepend_path("PATH",			root .. "/bin")

-------------------------
-- Include Directories --
-------------------------

prepend_path("CPATH",			root .. "/include")

------------------------------------
-- Build-Time Library Directories --
------------------------------------

prepend_path("LIBRARY_PATH",	root .. "/lib64")

----------------------------------
-- Run-Time Library Directories --
----------------------------------

prepend_path("LD_LIBRARY_PATH",	root .. "/lib64")


-- depends_on("gcc/.11.3.0_cuda")

