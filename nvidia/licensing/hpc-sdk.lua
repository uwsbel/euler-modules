help([[
For module documentation, please visit https://gitlab.com/uwsbel/euler-modules
]])

whatis("Loading this module verifies that you have read and accepted the terms of the \"NVIDIA SOFTWARE LICENSE AGREEMENT FOR HPC SOFTWARE DEVELOPMENT KIT\"")
whatis("[https://docs.nvidia.com/hpc-sdk/eula/index.html]")

if mode() == "load" then
	LmodMessage(
		"By using this module, you agree to the terms of the " ..
		"\"NVIDIA SOFTWARE LICENSE AGREEMENT FOR HPC SOFTWARE DEVELOPMENT KIT\".\n\n" .. 
		"The agreement is available at " .. 
		"[https://docs.nvidia.com/hpc-sdk/eula/index.html]\n\n" ..
		"If you do not agree, you must unload this module immediately."
	)
end
