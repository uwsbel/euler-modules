help([[
For module documentation, please visit https://gitlab.com/uwsbel/euler-modules
]])

whatis("Loading this module verifies that you have read and accepted the terms of NVIDIA's \"SOFTWARE DEVELOPER KITS, SAMPLES AND TOOLS LICENSE AGREEMENT\"")
whatis("[https://developer.download.nvidia.com/designworks/DesignWorks_SDKs_Samples_Tools_License_distrib_use_rights_2017_06_13.pdf]")

if mode() == "load" then
	LmodMessage(
		"By using this module, you agree to the terms of NVIDIA's " ..
		"\"SOFTWARE DEVELOPER KITS, SAMPLES AND TOOLS LICENSE AGREEMENT\".\n\n" .. 
		"The agreement is available at " .. 
		"[https://developer.download.nvidia.com/designworks/DesignWorks_SDKs_Samples_Tools_License_distrib_use_rights_2017_06_13.pdf]\n\n" ..
		"If you do not agree, you must unload this module immediately."
	)
end
