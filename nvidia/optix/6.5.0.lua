help([[
For module documentation, please visit https://gitlab.com/uwsbel/euler-modules
]])

local name = "optix"
local arch = string.gsub(subprocess("uname -m"), "%s+", "")
local vers = "6.5.0"
local root = "/opt/apps/" .. name .. "/" .. arch .. "/" .. vers

whatis("NVIDIA Optix SDK")
whatis("Version 6.5.0")

prereq("nvidia/licensing/sdk-samples-and-tools")

------------------------
-- Binary Directories --
------------------------

prepend_path("PATH",			root .. "/SDK-precompiled-samples")

-------------------------
-- Include Directories --
-------------------------

prepend_path("CPATH",			root .. "/include")

------------------------------------
-- Build-Time Library Directories --
------------------------------------

prepend_path("LIBRARY_PATH",	root .. "/lib64")

----------------------------------
-- Run-Time Library Directories --
----------------------------------

prepend_path("LD_LIBRARY_PATH",	root .. "/lib64")
