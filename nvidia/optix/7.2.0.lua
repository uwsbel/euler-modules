help([[
For module documentation, please visit https://gitlab.com/uwsbel/euler-modules
]])

local name = "optix"
local arch = string.gsub(subprocess("uname -m"), "%s+", "")
local vers = "7.2.0"
local root = "/opt/apps/" .. name .. "/" .. arch .. "/" .. vers

whatis("NVIDIA Optix SDK")
whatis("Version 7.2.0")

prereq("nvidia/licensing/sdk-samples-and-tools")

-------------------------
-- Include Directories --
-------------------------

prepend_path("CPATH",			root .. "/include")

