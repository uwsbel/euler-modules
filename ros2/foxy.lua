
help([[
For module documentation, please visit https://gitlab.com/uwsbel/euler-modules
]])

local name = "ros2"
local vers = "foxy"
local arch = "x86_64"
local root = "/opt/apps/" .. name .. "/" .. vers

whatis("The Robot Operating System (ROS)")
whatis("Version 2")
whatis("Foxy Fitzroy")

prepend_path("PATH", root .. "/bin")
prepend_path("LIBRARY_PATH", root .. "/lib")
prepend_path("LD_LIBRARY_PATH", root .. "/lib")
prepend_path("LIBRARY_PATH", root .. "/lib64")
prepend_path("LD_LIBRARY_PATH", root .. "/lib64")
prepend_path("INCLUDE_PATH", root .. "/include")
prepend_path("CPATH", root .. "/include")

prepend_path("COLCON_PREFIX_PATH", root)
prepend_path("CMAKE_PREFIX_PATH", root)
prepend_path("AMENT_PREFIX_PATH", root)
prepend_path("PKG_CONFIG_PATH", root .. "/lib/pkgconfig")
prepend_path("PYTHONPATH", root .. "/lib/python3.9/site-packages")

setenv("ROS_DISTRO", vers)
setenv("ROS_LOCALHOST_ONLY", 0)
setenv("ROS_PYTHON_VERSION", 3)
setenv("ROS_VERSION", 2)

-- Enable Fast-DDS backend
-- setenv("RMW_IMPLEMENTATION", "rmw_fastrtps_cpp")
