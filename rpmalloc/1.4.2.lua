
help([[
For module documentation, please visit https://gitlab.com/uwsbel/euler-modules
]])

local name = "rpmalloc"
local vers = "1.4.2"
local arch = string.gsub(subprocess("uname -m"), "%s+", "")
local root = "/opt/apps/" .. name .. "/" .. arch .. "/" .. vers

whatis("rpmalloc")
whatis("Version 1.4.2")


prepend_path("PATH",    root .. "/bin")
prepend_path("LIBRARY_PATH",    root .. "/lib")
prepend_path("LD_LIBRARY_PATH",    root .. "/lib")
