
help([[
For module documentation, please visit https://gitlab.com/uwsbel/euler-modules
]])

local name = "STAR-CCM+"
local vers = "18.04.009-R8"
local arch = string.gsub(subprocess("uname -m"), "%s+", "")
local root = "/opt/apps/" .. name .. "/" .. arch .. "/" .. vers

whatis("STAR-CCM+")
whatis("18.04.009-R8")


prepend_path("PATH",    root .. "/" .. vers .. "/" .. name .. vers .. "/star/bin")

