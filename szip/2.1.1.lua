
help([[
For help with modules, please contact euler-support@engr.wisc.edu
]])

local name = "szip"
local vers = "2.1.1"
local arch = string.gsub(subprocess("uname -m"), "%s+", "")
local root = "/opt/apps/" .. name .. "/" .. arch .. "/" .. vers


whatis("szip")
whatis("Version 2.1.1")


prepend_path("CPATH",             root .. "/include")
prepend_path("LD_LIBRARY_PATH",   root .. "/lib")
prepend_path("LIBRARY_PATH",      root .. "/lib")
prepend_path("CMAKE_PREFIX_PATH", root .. "/share/cmake")



